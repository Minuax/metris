package de.michl.metris.score;

import de.michl.metris.Metris;
import de.michl.metris.log.Logger;

import java.io.*;
import java.net.URL;
import java.util.TreeMap;

/**
 * Implemented by: Minuax
 * Implemented since version: 2.0.0
 * Date: 30.05.2020
 **/

public class ScoreHandler {

    private TreeMap<Integer, String> scoreList;

    public ScoreHandler() {
        this.scoreList = new TreeMap<>();

        readScore();
    }

    public void submitScore(String username, int score) {
        if (score == 0)
            return;

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((new URL("http://ni2883569-2.web17.nitrado.hosting/submit.php?user=" + username + "&score=" + score)).openStream()));
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readScore() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((new URL("http://ni2883569-2.web17.nitrado.hosting/readout.php")).openStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.startsWith("<"))
                    continue;

                if (line.isEmpty())
                    continue;

                this.scoreList.put(Integer.parseInt(line.split(":")[0]), line.split(":")[1]);
            }

            bufferedReader.close();
        } catch (IOException e) {
            Metris.metris.getLogger().warn("Could not read scores, please check your internet connection.");
        }
    }

    public TreeMap<Integer, String> getScoreList() {
        return scoreList;
    }
}
