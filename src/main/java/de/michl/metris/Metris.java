package de.michl.metris;

import de.michl.metris.gui.impl.GameGUI;
import de.michl.metris.gui.impl.TitleGUI;
import de.michl.metris.log.Logger;
import de.michl.metris.ressource.RessourceHandler;
import de.michl.metris.score.ScoreHandler;
import de.michl.metris.settings.SettingsHandler;
import de.michl.metris.web.AuthFrame;
import de.michl.metris.web.WebHandler;
import mdlaf.MaterialLookAndFeel;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Implemented by: Michl
 * Implemented since version: 1.0.0
 * Date: 22.05.2020
 **/

public class Metris {

    public static Metris metris; // Instanz der Hauptklasse

    private final String VERSION = "2.5.0";

    private final boolean latest;

    private final File gameDirectory;

    private final String playerName; // Variable zum Speichern des Spielernamen

    private int tries; // Variable zum Speichern der Versuche

    private final GameGUI gameGui; // GameGUI-Objekt deklinieren
    private final TitleGUI titleGUI; // TitleGUI-Objekt deklinieren

    private final JFrame jFrame; // JFrame-Objekt deklinieren

    private final Logger logger; // Logger-Objekt deklinieren

    private final SettingsHandler settingsHandler; // SettingsHandler-Objekt deklinieren
    private final RessourceHandler ressourceHandler; // RessourceHandler-Objekt deklinieren
    private final ScoreHandler scoreHandler; // ScoreHandler-Objekt deklinieren
    private final WebHandler webHandler;

    /**
     * Constructor, initialisiert das Spiel
     */
    public Metris(String playerName) {
        metris = this; // Instanziierung

        this.playerName = playerName; // Playername wird über Constructor definiert

        this.gameDirectory = new File(System.getenv("APPDATA"), "Metris"); // Dateiordner erstellen

        if (!gameDirectory.isDirectory()) { // Prüfen ob Dateiordner bereits existiert
            gameDirectory.mkdirs(); // ggf. Dateien erstellen
        }

        this.jFrame = new JFrame("Metris (kein billiger Tetris Klon, versprochen)"); // Erstellen des JFrame-Objekt
        this.jFrame.setSize(Toolkit.getDefaultToolkit().getScreenSize()); // Dimensionen setzen
        this.jFrame.setResizable(false); // Damit das Fenster nicht verändert werden kann, Skalierung problematisch
        this.jFrame.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
        this.jFrame.setLocationRelativeTo(null); // Fenster wird in der Mitte des Bildschirms erzeugt
        this.jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Damit die Game-Loop sich beim Schließen des Fensters beendet

        this.logger = new Logger(); // Logger-Objekt initialisieren

        this.settingsHandler = new SettingsHandler(); // SettingsHandler-Objekt initialisieren
        this.ressourceHandler = new RessourceHandler(); // RessourceHandler-Objekt initialisieren
        this.scoreHandler = new ScoreHandler(); // ScoreHandler-Objekt initialisieren
        this.webHandler = new WebHandler(); // WebHandler-Objekt initialisieren

        this.gameGui = new GameGUI(); // GameGUI-Objekt initialisieren
        this.titleGUI = new TitleGUI(); // TitleGUI-Objekt initialisieren

        this.jFrame.addMouseListener(this.titleGUI); // MouseListener für TitleGUI registrieren
        this.jFrame.addMouseMotionListener(this.titleGUI); // MouseMotionListener für TitleGUI registrieren

        this.jFrame.add(this.titleGUI); // TitleGUI zu JFrame hinzufügen

        this.jFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                System.exit(1337);
            }
        });

        BufferedImage icon = this.ressourceHandler.loadImage("/icon.png"); // BufferedImage für Icon erstellen

        this.jFrame.setIconImage(icon); // Icon setzen

        this.jFrame.setVisible(true); // JFrame sichtbar machen

        this.latest = !webHandler.checkVersion();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> { // Registrieren eines Shutdown-Hooks
            Metris.metris.getLogger().info("Shutdown Hook is running !");
            try {
                Metris.metris.getSettingsHandler().saveConfigurationFile(); // Speichern der Konfiguration
            } catch (IllegalAccessException e) {
                Metris.metris.getLogger().warn("Error saving configuration file.");
            }
        }));
        Metris.metris.getLogger().info("Application Terminating ...");
    }

    /**
     * Methode um aus dem Spiel ins Hauptmenü zurückzukehren
     */
    public void backToMenu() {
        upTries();
        this.gameGui.stopGame(); // GameGUI::stopGame() aufrufen (Gameloop und Musik stoppen)

        this.jFrame.removeKeyListener(this.gameGui);
        this.jFrame.removeMouseListener(this.gameGui);
        this.jFrame.remove(this.gameGui); // GameGUI aus JFrame entfernen

        this.jFrame.add(this.titleGUI); // TitleGUI zu JFrame hinzufügen
        this.jFrame.addMouseListener(this.titleGUI); // MouseListener für TitleGUI registrieren

        this.titleGUI.repaint(); // Rendering des TitleGUI updaten
    }

    /**
     * Methode um das Spiel zu starten
     *
     * @param titleGUI TitleGUI
     */
    public void startGame(TitleGUI titleGUI) {
        this.jFrame.remove(titleGUI); // TitleGUI von JFrame entfernen

        this.jFrame.addMouseMotionListener(this.gameGui); // MouseMotionListener für GameGUI registrieren
        this.jFrame.addMouseListener(this.gameGui); // MouseListener für GameGUI registrieren
        if (this.jFrame.getKeyListeners().length == 0) {
            this.jFrame.addKeyListener(gameGui); // KeyListener für GameGUI registrieren
        } else {
            Metris.metris.getLogger().warn("Keylistener already registered.");
        }

        this.jFrame.add(this.gameGui); // GameGUI zu JFrame hinzufügen

        this.gameGui.startGame(); // GameGUI::startGame() aufrufen
        this.gameGui.startMusic(); // GameGUI::startMusic() aufrufen
        this.gameGui.revalidate(); // JPanel::revalidate() aufrufen
    }

    /**
     * Gibt die Breite des Fensters zurück
     *
     * @return WIDTH
     */
    public int getWidth() {
        return Toolkit.getDefaultToolkit().getScreenSize().width;
    }

    /**
     * Gibt die Höhe des Fensters zurück
     *
     * @return HEIGHT
     */
    public int getHeight() {
        return Toolkit.getDefaultToolkit().getScreenSize().height;
    }

    /**
     * Gibt die Logger-Instanz zurück
     *
     * @return logger
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * Gibt die RessourceHandler-Instanz zurück
     *
     * @return ressourceHandler
     */
    public RessourceHandler getRessourceHandler() {
        return ressourceHandler;
    }

    /**
     * Gibt den Spielernamen zurück
     *
     * @return playerName
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * Gibt die Version zurück
     *
     * @return VERSION
     */
    public String getVersion() {
        return VERSION;
    }

    /**
     * Gibt das Spieldirectory zurück
     *
     * @return gameDirectory
     */
    public File getGameDirectory() {
        return gameDirectory;
    }

    /**
     * Gibt die Versuche zurück
     *
     * @return tries
     */
    public int getTries() {
        return tries;
    }

    /**
     * Erhöht die Versuche um 1
     */
    public void upTries() {
        this.tries += 1;
    }

    /**
     * Gibt die GameGUI-Instanz zurück
     *
     * @return gameGui
     */
    public GameGUI getGameGui() {
        return gameGui;
    }

    /**
     * Gibt die ScoreHandler-Instanz zurück
     *
     * @return scoreHandler
     */
    public ScoreHandler getScoreHandler() {
        return scoreHandler;
    }

    /**
     * Gibt die SettingsHandler-Instanz zurück
     *
     * @return settingsHandler
     */
    public SettingsHandler getSettingsHandler() {
        return settingsHandler;
    }

    /**
     * Gibt den WebHandler Zurück
     *
     * @return webHandler
     */
    public WebHandler getWebHandler() {
        return webHandler;
    }

    /**
     * Gibt latest zurück
     *
     * @return latest
     */
    public boolean isLatest() {
        return latest;
    }

    /**
     * Hauptmethode des Spiels, Einstiegspunkt des Programms
     *
     * @param args Startargumente
     */
    public static void main(String[] args) {
        if (args.length > 0) { // Sind Startargumente vorhanden?
            if (args[0].equalsIgnoreCase("-debug")) { // Lautet das erste Startargument '-debug'?
                Logger.debug = true; // Logger in Debugmodus versetzen
            }
        }

        System.out.println("--- Willkommen bei Metris ---"); // Splashscreen


        try {
            UIManager.setLookAndFeel(new MaterialLookAndFeel()); // Material-Look anwenden
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace(); // Bei Fehler: Berichten
        }

        if (Logger.debug) { // Debugmodus aktiviert?
            System.out.println("Debugmode aktiviert.");
            new Metris("DEBUG"); // Metris im Debugmodus starten
        } else {
            new AuthFrame(); // JFrame zum einloggen instanziieren
        }

        System.out.println("--- Viel Spaß ---");
    }
}

