package de.michl.metris.settings;

import de.michl.metris.Metris;
import de.michl.metris.log.Logger;
import org.json.JSONObject;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implemented by: Minuax
 * Implemented since version: 2.2.1
 * Date: 02.06.2020
 **/

public class SettingsHandler {

    private final File configurationDirectory;
    private final File configurationFile;

    private final HashMap<String, Field> optionList;

    public float MUSIC_VOLUME = 0.1f;
    public boolean GRIDRECT = true, RAINBOW = false, CUSTOM_BACKGROUD = false;
    public String BACKGROUND_FILENAME = "";

    public SettingsHandler() {
        this.configurationDirectory = new File(Metris.metris.getGameDirectory(), "config");

        if (!this.configurationDirectory.isDirectory()) {
            this.configurationDirectory.mkdirs();
        }

        this.configurationFile = new File(this.configurationDirectory, "config.json");

        this.optionList = new HashMap<>();

        try {
            for (Field field : getFields()) {
                optionList.put(field.getName(), field);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            if (configurationFile.exists()) {
                loadConfiguration();
                Metris.metris.getLogger().info("Loading configuration.");
            } else {
                createConfigurationFile();
                Metris.metris.getLogger().info("No configuration present, creating one.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createConfigurationFile() throws IllegalAccessException {
        JSONObject main = new JSONObject();
        for (String s : this.optionList.keySet()) {
            main.put(s, this.optionList.get(s).get(this));
            Metris.metris.getLogger().info(s + " was added.");
        }

        try {
            BufferedWriter br = new BufferedWriter(new FileWriter(configurationFile.getAbsolutePath()));
            br.write(main.toString());
            br.close();
        } catch (IOException e) {
            Metris.metris.getLogger().warn("Error creating configuration file.");
        }
    }

    public void saveConfigurationFile() throws IllegalAccessException {
        JSONObject main = new JSONObject();
        for (String s : this.optionList.keySet()) {
            main.put(s, this.optionList.get(s).get(this));
            Metris.metris.getLogger().info(s + " was saved.");
        }
        try {
            BufferedWriter br = new BufferedWriter(new FileWriter(configurationFile.getAbsolutePath()));
            br.write(main.toString());
            br.close();
        } catch (IOException e) {
            Metris.metris.getLogger().warn("Error saving configuration file.");
        }
    }

    public void loadConfiguration() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(configurationFile.getAbsolutePath()));
            String out = br.lines().collect(Collectors.joining("\n"));

            JSONObject obj = new JSONObject(out);

            for (String s : this.optionList.keySet()) {
                if (obj.get(s) != null) {
                    if (this.optionList.get(s).getType().isAssignableFrom(Double.TYPE)) {
                        this.optionList.get(s).set(this, Double.valueOf(obj.get(s).toString()));
                        Metris.metris.getLogger().info(s + " was set to " + obj.get(s) + ". (Double)");
                    } else if (this.optionList.get(s).getType().isAssignableFrom(Float.TYPE)) {
                        this.optionList.get(s).set(this, Float.valueOf(obj.get(s).toString()));
                        Metris.metris.getLogger().info(s + " was set to " + obj.get(s) + ". (Float)");
                    } else if (this.optionList.get(s).getType().isAssignableFrom(Integer.TYPE)) {
                        this.optionList.get(s).set(this, Integer.valueOf(obj.get(s).toString()));
                        Metris.metris.getLogger().info(s + " was set to " + obj.get(s) + ". (Integer)");
                    } else if (this.optionList.get(s).getType().isAssignableFrom(Boolean.TYPE)) {
                        this.optionList.get(s).set(this, Boolean.valueOf(obj.get(s).toString()));
                        Metris.metris.getLogger().info(s + " was set to " + obj.get(s) + ". (Boolean)");
                    } else {
                        this.optionList.get(s).set(this, obj.get(s).toString());
                        Metris.metris.getLogger().info(s + " was set to " + obj.get(s) + ". (String)");
                    }
                }
            }

        } catch (Exception ex) {
            Metris.metris.getLogger().warn("Error loading configuration file.");
        }
    }

    public List<Field> getFields() {
        List<Field> fields = new ArrayList<>();
        for (Field field : getClass().getFields()) {
            field.setAccessible(true);
            fields.add(field);
        }
        return fields;
    }
}
