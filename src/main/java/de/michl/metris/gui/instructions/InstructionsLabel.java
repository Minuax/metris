package de.michl.metris.gui.instructions;

import de.michl.metris.Metris;

import java.awt.*;

public class InstructionsLabel {

    private String headline, description, key; // Variable für den Button-Text

    private int buttonX, buttonY; // Variabeln für die ID und Position des Button

    private boolean buttonHovered; // Maus über Knopf

    /**
     * Constructor für Button.
     *
     * @param headline
     * @param buttonX
     * @param buttonY
     * @param description
     * @param key
     */
    public InstructionsLabel(String headline, String description, String key, int buttonX, int buttonY) {
        this.headline = headline;
        this.description = description;
        this.key = key;
        this.buttonX = buttonX;
        this.buttonY = buttonY;
    }

    /**
     * Rendermethode für Button
     *
     * @param graphics
     */
    public void renderButton(Graphics graphics) {
        graphics.setColor(new Color(0, 0, 0, 180)); // Farbe auf 'buttonColor'
        graphics.fillRect(buttonX, buttonY, 300, 60); // Rechteck rendern

        graphics.setColor(Color.WHITE); // Farbe auf weiß
        graphics.setFont(new Font("Verdana", Font.BOLD, 20)); // Schriftart ändern
        graphics.drawString(headline + " (" + key + ")", buttonX + 10, buttonY + 40 / 2 + 5); // Text rendern
        graphics.setColor(Color.LIGHT_GRAY);
        graphics.setFont(new Font("Verdana", Font.BOLD, 15)); // Schriftart ändern
        graphics.drawString(description, buttonX + 10, buttonY + 40 / 2 + 30);
    }

    /**
     * Methode um Hovering zu erkennen
     *
     * @param mouseX
     * @param mouseY
     */
    public void setHovered(int mouseX, int mouseY) {
        this.buttonHovered = mouseX >= buttonX && mouseX <= buttonX + 200 && mouseY >= buttonY && mouseY <= buttonY + 40;
    }

    /**
     * Gibt buttonHovered zurück
     *
     * @return buttonHovered
     */
    public boolean isButtonHovered() {
        return buttonHovered;
    }

}
