package de.michl.metris.gui.impl;

import de.michl.metris.Metris;
import de.michl.metris.gui.GUI;
import de.michl.metris.gui.button.Button;
import de.michl.metris.gui.instructions.InstructionsLabel;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Implemented by: Minuax
 * Implemented since version: 1.0.0
 * Date: 22.05.2020
 **/

public class TitleGUI extends GUI {

    private BufferedImage titleAsset, instructionsAsset;
    private boolean showHighscore, showBest, showInstructions, showBackground, showChangelog;

    private Button backButton, selectBackground, standartBackground;
    private InstructionsLabel moveLeft, moveRight, turn, speedUp, pause, rainbow, grid, volumeUp, volumeDown, songOne, songTwo, songThree;

    private List<String> changeLog;

    public TitleGUI() {
        Metris.metris.getLogger().info("TitleGUI wurde erfolgreich initialisiert.");
        loadAssets();

        this.buttonList.add(new Button(0, "Start", Metris.metris.getWidth() / 2 - 205, Metris.metris.getHeight() / 2 - 140, 200, 60, new Color(33, 82, 167)));
        this.buttonList.add(new Button(1, "HighScore", Metris.metris.getWidth() / 2 + 5, Metris.metris.getHeight() / 2 - 140, 200, 60, new Color(33, 82, 167)));
        this.buttonList.add(new Button(2, "Beste", Metris.metris.getWidth() / 2 - 205, Metris.metris.getHeight() / 2 - 70, 200, 60, new Color(33, 82, 167)));
        this.buttonList.add(new Button(3, "Steuerung", Metris.metris.getWidth() / 2 + 5, Metris.metris.getHeight() / 2 - 70, 200, 60, new Color(33, 82, 167)));
        this.buttonList.add(new Button(4, "Hintergrund", Metris.metris.getWidth() / 2 - 205, Metris.metris.getHeight() / 2, 200, 60, new Color(33, 82, 167)));

        this.buttonList.add(new Button(5, "Beenden", Metris.metris.getWidth() / 2 + 5, Metris.metris.getHeight() / 2, 200, 60, new Color(230, 0, 0)));


        this.buttonList.add(standartBackground = new Button(7, "Standard", Metris.metris.getWidth() / 2 - 200 / 2, Metris.metris.getHeight() / 2 - 40, 200, 60, new Color(33, 82, 167)));
        this.buttonList.add(selectBackground = new Button(8, "Durchsuchen", Metris.metris.getWidth() / 2 - 200 / 2, Metris.metris.getHeight() / 2 + 40, 200, 60, new Color(33, 82, 167)));

        this.buttonList.add(backButton = new Button(6, "Zurück", Metris.metris.getWidth() / 2 - 200 / 2, Metris.metris.getHeight() - 140, 200, 60, new Color(33, 82, 167)));

        this.moveLeft = new InstructionsLabel("Links", "Bewegt die Form nach links", "Pfeiltaste links", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 - 420);
        this.moveRight = new InstructionsLabel("Rechts", "Bewegt die Form nach rechts", "Pfeiltaste rechts", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 - 360);
        this.turn = new InstructionsLabel("Drehen", "Dreh die Form", "Pfeiltaste hoch", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 - 300);
        this.speedUp = new InstructionsLabel("Schnell", "Beschleunigt den Fall", "Pfeiltaste runter", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 - 240);
        this.pause = new InstructionsLabel("Pause", "Pausiert das Spiel", "ESC", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 - 180);
        this.rainbow = new InstructionsLabel("Regenbogen", "Aktiviert Farbeffekte", "C", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 - 120);
        this.grid = new InstructionsLabel("Grid", "Aktiviert Grid-Hilfe", "J", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 - 60);
        this.volumeUp = new InstructionsLabel("Lautstärke++", "Stellt Musik lauter", "+", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2);
        this.volumeDown = new InstructionsLabel("Lautstärke--", "Stellt Musik leiser", "-", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 + 60);
        this.songOne = new InstructionsLabel("Lied 1", "Spielt Lied 1", "1", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 + 120);
        this.songTwo = new InstructionsLabel("Lied 2", "Spielt Lied 2", "2", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 + 180);
        this.songThree = new InstructionsLabel("Lied 3", "Spielt Lied 3", "3", Metris.metris.getWidth() / 2 - 300 / 2, Metris.metris.getHeight() / 2 + 240);


        this.changeLog = Metris.metris.getWebHandler().readChangelog();
    }

    public void loadAssets() {
        titleAsset = Metris.metris.getRessourceHandler().loadImage("/title.png");
        instructionsAsset = Metris.metris.getRessourceHandler().loadImage("/instructions.png");
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawImage(Metris.metris.getRessourceHandler().getBackgroundAsset(), 0, 0, null);

        if (showHighscore) {
            g.setColor(new Color(0, 0, 0, 190));
            g.fillRect(Metris.metris.getWidth() / 2 - 220, Metris.metris.getHeight() / 2 - 270, 440, 540);


            g.setColor(Color.WHITE);
            g.setFont(new Font("Verdana", Font.BOLD, 40));
            if (Metris.metris.getScoreHandler().getScoreList().lastEntry() != null) {
                g.drawString("HIGHSCORE", Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("HIGHSCORE") / 2, Metris.metris.getHeight() / 2 - 100);

                g.setColor(GameGUI.rainbow(1, 1));
                g.setFont(new Font("Verdana", Font.BOLD, 50));
                g.drawString(Metris.metris.getScoreHandler().getScoreList().lastEntry().getValue() + ": " + Metris.metris.getScoreHandler().getScoreList().lastEntry().getKey(), Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth(Metris.metris.getScoreHandler().getScoreList().lastEntry().getValue() + ": " + Metris.metris.getScoreHandler().getScoreList().lastEntry().getKey()) / 2, Metris.metris.getHeight() / 2);
            } else {
                g.drawString("Nothing here..", 50, 50);
            }

            backButton.renderButton(g);
        } else if (showBest) {
            g.setColor(new Color(0, 0, 0, 190));
            g.fillRect(Metris.metris.getWidth() / 2 - 220, Metris.metris.getHeight() / 2 - 270, 440, 540);


            g.setColor(Color.WHITE);
            g.setFont(new Font("Verdana", Font.BOLD, 40));
            if (Metris.metris.getScoreHandler().getScoreList().lastEntry() != null) {
                g.drawString("Bestenliste", Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Bestenliste") / 2, Metris.metris.getHeight() / 2 - 100);

                g.setColor(Color.WHITE);
                g.setFont(new Font("Verdana", Font.BOLD, 20));

                int ammount = 0;
                int yOff = 0;
                for (int i : Metris.metris.getScoreHandler().getScoreList().descendingKeySet()) {
                    if (ammount <= 5)
                        g.drawString(Metris.metris.getScoreHandler().getScoreList().get(i) + ": " + i, Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth(Metris.metris.getScoreHandler().getScoreList().get(i) + ": " + i) / 2, Metris.metris.getHeight() / 2 + yOff - 60);

                    ammount++;
                    yOff += 40;
                }

            } else {
                g.drawString("Nothing here..", 50, 50);
            }

            backButton.renderButton(g);
        } else if (showInstructions) {
            g.setColor(Color.WHITE);
            g.setFont(new Font("Verdana", Font.BOLD, 40));

            g.drawString("Steuerung", Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Steuerung") / 2, 80);

            moveLeft.renderButton(g);
            moveRight.renderButton(g);
            turn.renderButton(g);
            speedUp.renderButton(g);
            pause.renderButton(g);
            rainbow.renderButton(g);
            grid.renderButton(g);
            volumeUp.renderButton(g);
            volumeDown.renderButton(g);
            songOne.renderButton(g);
            songTwo.renderButton(g);
            songThree.renderButton(g);

            backButton.renderButton(g);
        } else if (showBackground) {
            g.setColor(new Color(0, 0, 0, 190));
            g.fillRect(Metris.metris.getWidth() / 2 - 220, Metris.metris.getHeight() / 2 - 270, 440, 540);


            g.setColor(Color.WHITE);
            g.setFont(new Font("Verdana", Font.BOLD, 40));

            g.drawString("Hintergrund", Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Hintergrund") / 2, Metris.metris.getHeight() / 2 - 100);

            backButton.renderButton(g);
            standartBackground.renderButton(g);
            selectBackground.renderButton(g);
        } else if (showChangelog) {
            g.setColor(Color.WHITE);
            g.setFont(new Font("Verdana", Font.BOLD, 40));

            g.drawString("Changelog", Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Changelog") / 2, 50);

            g.setColor(new Color(0, 0, 0, 190));
            g.fillRect(Metris.metris.getWidth() / 2 - 220, Metris.metris.getHeight() / 2 - 270, 440, 540);


            g.setColor(Color.WHITE);
            g.setFont(new Font("Verdana", Font.PLAIN, 18));

            g.setColor(Color.WHITE);
            g.drawString("=Added", 25, 100);

            g.setColor(Color.GREEN);
            g.fillRect(5, 85, 17, 17);

            g.setColor(Color.WHITE);
            g.drawString("=Removed", 25, 120);

            g.setColor(Color.RED);
            g.fillRect(5, 105, 17, 17);

            g.setColor(Color.WHITE);
            g.drawString("=Updated", 25, 140);

            g.setColor(new Color(26, 134, 203, 255));
            g.fillRect(5, 125, 17, 17);

            g.setColor(Color.WHITE);

            int yOffset = -40;
            for (String s : this.changeLog) {
                if (s.isEmpty()) {
                    yOffset += 15;
                    continue;
                }

                String prefix = s.split(":")[0];
                String text = s.split(":")[1];

                switch (prefix) {
                    case "[TITLE]":
                        g.setColor(Color.WHITE);
                        g.setFont(new Font("Verdana", Font.BOLD, 18));
                        g.drawString(text, Metris.metris.getWidth() / 2 - 195, Metris.metris.getHeight() / 2 - 200 + yOffset);
                        break;
                    case "[ADDED]":
                        g.setFont(new Font("Verdana", Font.PLAIN, 16));
                        g.drawString(text, Metris.metris.getWidth() / 2 - 180, Metris.metris.getHeight() / 2 - 200 + yOffset);
                        g.setColor(Color.GREEN);
                        g.fillRect(Metris.metris.getWidth() / 2 - 195, Metris.metris.getHeight() / 2 - 213 + yOffset, 13, 13);
                        g.setColor(Color.WHITE);
                        break;
                    case "[UPDATED]":
                        g.setFont(new Font("Verdana", Font.PLAIN, 16));
                        g.drawString(text, Metris.metris.getWidth() / 2 - 180, Metris.metris.getHeight() / 2 - 200 + yOffset);
                        g.setColor(new Color(26, 134, 203, 255));
                        g.fillRect(Metris.metris.getWidth() / 2 - 195, Metris.metris.getHeight() / 2 - 213 + yOffset, 13, 13);
                        g.setColor(Color.WHITE);
                        break;
                    case "[REMOVED]":
                        g.setFont(new Font("Verdana", Font.PLAIN, 16));
                        g.drawString(text, Metris.metris.getWidth() / 2 - 180, Metris.metris.getHeight() / 2 - 200 + yOffset);
                        g.setColor(Color.RED);
                        g.fillRect(Metris.metris.getWidth() / 2 - 195, Metris.metris.getHeight() / 2 - 213 + yOffset, 13, 13);
                        g.setColor(Color.WHITE);
                        break;
                    default:
                }

                yOffset += 20;

            }

            backButton.renderButton(g);
        } else {
            g.setColor(new Color(0, 0, 0, 190));
            g.fillRect(Metris.metris.getWidth() / 2 - 220, Metris.metris.getHeight() / 2 - 270, 440, 360);


            g.setColor(Color.WHITE);
            g.setFont(new Font("Verdana", Font.BOLD, 10));
            g.drawString("Eingeloggt als: " + Metris.metris.getPlayerName(), Metris.metris.getWidth() - g.getFontMetrics().stringWidth("Eingeloggt als: " + Metris.metris.getPlayerName()) - 15, 15);

            for (Button button : this.buttonList) {
                if (button.getButtonID() < 6)
                    button.renderButton(g);
            }

        }

        if (!showChangelog && !showInstructions)
            g.drawImage(titleAsset, Metris.metris.getWidth() / 2 - titleAsset.getWidth() / 2, Metris.metris.getHeight() / 2 - titleAsset.getHeight() / 2 - 200, null);

        g.setColor(Color.WHITE);
        g.setFont(new Font("Verdana", Font.BOLD, 10));

        if (!showChangelog) {
            g.drawString("Version " + Metris.metris.getVersion() + " Changelog", 10, 15);
            g.drawLine(14 + g.getFontMetrics().stringWidth("Version " + Metris.metris.getVersion()), 17, 13 + g.getFontMetrics().stringWidth("Version " + Metris.metris.getVersion()) + g.getFontMetrics().stringWidth("Changelog"), 17);
        }

        g.drawString("Eingeloggt als: " + Metris.metris.getPlayerName(), Metris.metris.getWidth() - g.getFontMetrics().stringWidth("Eingeloggt als: " + Metris.metris.getPlayerName()) - 15, 15);


        if (!Metris.metris.isLatest()) {
            g.setColor(GameGUI.rainbow(1, 1));

            g.setFont(new Font("Verdana", Font.BOLD, 25));
            g.drawString("Neue Version verfügbar!", Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Neue Version verfügbar!") / 2, Metris.metris.getHeight() - 50);
        }

        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getX() >= 0 && e.getX() <= 160 && e.getY() >= 0 && e.getY() <= 65) {
            if (!showChangelog && !showBackground && !showInstructions && !showBest || !showHighscore) {
                this.changeLog = Metris.metris.getWebHandler().readChangelog();
                showChangelog = true;
            }
        }

        if (e.getX() >= Metris.metris.getWidth() / 2 - 200 && e.getX() <= Metris.metris.getWidth() / 2 + 200 && e.getY() >= Metris.metris.getHeight() - 80 && e.getY() <= Metris.metris.getHeight()) {
            if (!showChangelog && !showBackground && !showInstructions && !showBest || !showHighscore) {
                if (!Metris.metris.isLatest()) {
                    try {
                        Desktop.getDesktop().browse(new URI("http://ni2883569-2.web17.nitrado.hosting/Metris.jar"));
                    } catch (IOException | URISyntaxException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        }
        if (this.isShowing()) {
            for (Button button : this.buttonList) {
                if (button.isButtonHovered()) {
                    repaint();
                    if (button.getButtonID() == 0) {
                        if (!showHighscore && !showBest && !showInstructions && !showBackground && !showChangelog)
                            Metris.metris.startGame(this);
                    } else if (button.getButtonID() == 1) {
                        if (!showHighscore && !showBest && !showInstructions && !showBackground && !showChangelog) {
                            Metris.metris.getScoreHandler().readScore();
                            showHighscore = true;
                        }
                    } else if (button.getButtonID() == 2) {
                        if (!showHighscore && !showBest && !showInstructions && !showBackground && !showChangelog) {
                            Metris.metris.getScoreHandler().readScore();
                            showBest = true;
                        }
                    } else if (button.getButtonID() == 3) {
                        if (!showHighscore && !showBest && !showInstructions && !showBackground && !showChangelog)
                            showInstructions = true;
                    } else if (button.getButtonID() == 4) {
                        if (!showHighscore && !showBest && !showInstructions && !showBackground && !showChangelog)
                            showBackground = true;
                    } else if (button.getButtonID() == 5) {
                        if (!showHighscore && !showBest && !showInstructions && !showBackground && !showChangelog)
                            System.exit(1337);
                    } else if (button.getButtonID() == 6) {
                        if (showHighscore || showBest || showInstructions || showBackground || showChangelog) {
                            showHighscore = false;
                            showBest = false;
                            showInstructions = false;
                            showBackground = false;
                            showChangelog = false;
                        }
                    } else if (button.getButtonID() == 7) {
                        if (showBackground) {
                            Metris.metris.getRessourceHandler().loadStandartBackground();
                            Metris.metris.getSettingsHandler().CUSTOM_BACKGROUD = false;
                        }
                    } else if (button.getButtonID() == 8) {
                        if (showBackground) {
                            JFileChooser jFileChooser = new JFileChooser();
                            jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Bilder", "jpg", "png"));

                            int showOpenDialog = jFileChooser.showOpenDialog(null);

                            File file = null;

                            if (showOpenDialog == JFileChooser.APPROVE_OPTION) {
                                file = jFileChooser.getSelectedFile();
                            }

                            if (file != null) {
                                File customFile = new File(Metris.metris.getGameDirectory(), file.getName());

                                try {
                                    FileUtils.copyFile(file, customFile);
                                } catch (IOException ioException) {
                                    ioException.printStackTrace();
                                }


                                Metris.metris.getRessourceHandler().loadCustomBackground(customFile);
                            }
                        }
                    }
                }
            }
        }
    }

}
