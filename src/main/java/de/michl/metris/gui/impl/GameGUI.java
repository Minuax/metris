package de.michl.metris.gui.impl;

import de.michl.metris.Metris;
import de.michl.metris.gui.GUI;
import de.michl.metris.gui.button.Button;
import de.michl.metris.log.Logger;
import de.michl.metris.shape.Shape;

import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * Implemented by: Minuax
 * Implemented since version: 1.0.0
 * Date: 22.05.2020
 **/

public class GameGUI extends GUI {

    //Assets
    private Clip music;

    private BufferedImage blockAsset, pause;

    private final int boardHeight = 20, boardWidth = 10;

    private final int blockSize = 30;

    private final int[][] field = new int[boardHeight][boardWidth];

    private final Shape[] shapes = new Shape[7];

    private Shape currentShape, nextShape;
    private int lastIndex;

    private Timer looper;

    private final int delay = 1000 / 60;

    private boolean gamePaused = false, gameOver = false, submitted = false;

    private int score = 0;

    public GameGUI() {
        Metris.metris.getLogger().info("GameGUI wurde erfolgreich initialisiert.");

        loadAssets();

        this.buttonList.add(new Button(0, "Fortfahren", Metris.metris.getWidth() / 2 - 100, Metris.metris.getHeight() / 2 - 45, 200, 60, new Color(33, 82, 167)));
        this.buttonList.add(new Button(1, "Neustart", Metris.metris.getWidth() / 2 - 100, Metris.metris.getHeight() / 2 + 20, 200, 60, new Color(33, 82, 167)));
        this.buttonList.add(new Button(3, "Hauptmenü", Metris.metris.getWidth() / 2 - 100, Metris.metris.getHeight() / 2 + 85, 200, 60, new Color(33, 82, 167)));
        this.buttonList.add(new Button(2, "Beenden", Metris.metris.getWidth() / 2 - 100, Metris.metris.getHeight() / 2 + 160, 200, 60, new Color(230, 0, 0)));

        // create game looper
        looper = new Timer(delay, new GameLooper());

        // create shapes

        shapes[0] = new Shape(new int[][]{
                {1, 1, 1, 1}   // I shape;
        }, blockAsset.getSubimage(0, 0, blockSize, blockSize), this, 1);

        shapes[1] = new Shape(new int[][]{
                {1, 1, 1},
                {0, 1, 0},   // T shape;
        }, blockAsset.getSubimage(blockSize, 0, blockSize, blockSize), this, 2);

        shapes[2] = new Shape(new int[][]{
                {1, 1, 1},
                {1, 0, 0},   // L shape;
        }, blockAsset.getSubimage(blockSize * 2, 0, blockSize, blockSize), this, 3);

        shapes[3] = new Shape(new int[][]{
                {1, 1, 1},
                {0, 0, 1},   // J shape;
        }, blockAsset.getSubimage(blockSize * 3, 0, blockSize, blockSize), this, 4);

        shapes[4] = new Shape(new int[][]{
                {0, 1, 1},
                {1, 1, 0},   // S shape;
        }, blockAsset.getSubimage(blockSize * 4, 0, blockSize, blockSize), this, 5);

        shapes[5] = new Shape(new int[][]{
                {1, 1, 0},
                {0, 1, 1},   // Z shape;
        }, blockAsset.getSubimage(blockSize * 5, 0, blockSize, blockSize), this, 6);

        shapes[6] = new Shape(new int[][]{
                {1, 1},
                {1, 1},   // O shape;
        }, blockAsset.getSubimage(blockSize * 6, 0, blockSize, blockSize), this, 7);


    }

    public void loadAssets() {
        blockAsset = Metris.metris.getRessourceHandler().loadImage("/tiles.png");
        pause = Metris.metris.getRessourceHandler().loadImage("/pause.png");
    }

    public void startMusic() {
        if (music == null || gameOver) {
            int randomTrack = new Random().nextInt(2);

            switch (randomTrack) {
                case 0:
                    music = Metris.metris.getRessourceHandler().loadSound("/music0.wav");
                    break;
                case 1:
                    music = Metris.metris.getRessourceHandler().loadSound("/music1.wav");
                    break;
                case 2:
                    music = Metris.metris.getRessourceHandler().loadSound("/music2.wav");
                    break;
                default:
                    music = Metris.metris.getRessourceHandler().loadSound("/music0.wav");
            }

            music.loop(Clip.LOOP_CONTINUOUSLY);

        }
    }

    private void update() {
        if (gameOver) {
            music.stop();
        } else {
            if (!music.isActive()) {
                music.start();
            }
        }

        if (gamePaused || gameOver) {
            return;
        }

        currentShape.update();
        setVolume(Metris.metris.getSettingsHandler().MUSIC_VOLUME);
    }


    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(Metris.metris.getRessourceHandler().getBackgroundAsset(), 0, 0, null);

        g.setColor(new Color(0, 0, 0, 100));
        g.fillRect(Metris.metris.getWidth() / 2 - boardWidth * blockSize / 2, Metris.metris.getHeight() / 2 - boardHeight * blockSize / 2, boardWidth * blockSize, boardHeight * blockSize);

        for (int row = 0; row < field.length; row++) {
            for (int col = 0; col < field[row].length; col++) {
                if (field[row][col] != 0) {
                    g.drawImage(blockAsset.getSubimage(((field[row][col] - 1) * blockSize), 0, blockSize, blockSize), (Metris.metris.getWidth() / 2 - boardWidth * blockSize / 2) + col * blockSize, Metris.metris.getHeight() / 2 - Metris.metris.getGameGui().getBoardHeight() * Metris.metris.getGameGui().getBlockSize() / 2 + row * blockSize, null);
                }
            }
        }

        if (!gameOver) {
            for (int row = 0; row < nextShape.getCoordinates().length; row++) {
                for (int col = 0; col < nextShape.getCoordinates()[0].length; col++) {
                    if (nextShape.getCoordinates()[row][col] != 0) {
                        g.drawImage(nextShape.getBlock(), (Metris.metris.getWidth() / 2 - (nextShape.getCoordinates()[0].length * Metris.metris.getGameGui().getBlockSize()) / 2) + col * 30, row * 30 + 50, null);
                    }
                }
            }
        }

        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(1));
        g2d.setColor(Metris.metris.getSettingsHandler().RAINBOW ? rainbow(1, 1) : new Color(0, 0, 0, 100));

        for (int i = 0; i <= boardHeight; i++) {
            if (Metris.metris.getSettingsHandler().RAINBOW) {
                g2d.setColor(rainbow(i * 200000000L, 1f));
            }

            g2d.drawLine(Metris.metris.getWidth() / 2 - boardWidth * blockSize / 2, Metris.metris.getHeight() / 2 - boardHeight * blockSize / 2 + i * blockSize, Metris.metris.getWidth() / 2 + boardWidth * blockSize / 2, Metris.metris.getHeight() / 2 - boardHeight * blockSize / 2 + i * blockSize);
        }

        for (int j = 0; j <= boardWidth; j++) {
            if (Metris.metris.getSettingsHandler().RAINBOW) {
                g2d.setColor(rainbow(j * 200000000L, 1f));
            }
            g2d.drawLine(Metris.metris.getWidth() / 2 - boardWidth * blockSize / 2 + j * blockSize, Metris.metris.getHeight() / 2 - boardHeight * blockSize / 2, Metris.metris.getWidth() / 2 - boardWidth * blockSize / 2 + j * blockSize, Metris.metris.getHeight() / 2 - boardHeight * blockSize / 2 + boardHeight * blockSize);
        }


        currentShape.render(g);

        if (gamePaused) {
            g.setColor(new Color(0, 0, 0, 150));

            g.fillRect(0, 0, Metris.metris.getWidth(), Metris.metris.getHeight());

            g.drawImage(pause, Metris.metris.getWidth() / 2 - pause.getWidth() / 2, Metris.metris.getHeight() / 2 - pause.getHeight() / 2 - 200, null);

            g.setColor(Color.WHITE);
            g.setFont(new Font("Verdana", Font.BOLD, 30));
            g.drawString(Metris.metris.getPlayerName(), Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth(Metris.metris.getPlayerName()) / 2, Metris.metris.getHeight() / 2 - 100);
            g.drawString("Score: " + Metris.metris.getGameGui().getScore(), Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Score: " + Metris.metris.getGameGui().getScore()) / 2, Metris.metris.getHeight() / 2 - 70);
            g.drawString("Versuche: " + Metris.metris.getTries(), Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Versuche: " + Metris.metris.getTries()) / 2, Metris.metris.getHeight() / 2);

            for (Button button : this.buttonList) {
                button.renderButton(g);
            }
        }

        if (gameOver) {
            g.setColor(new Color(0, 0, 0, 150));
            g.fillRect(0, 0, Metris.metris.getWidth(), Metris.metris.getHeight());
            g.setColor(Color.RED);
            g.setFont(new Font("Verdana", Font.BOLD, 60));
            g.drawString("GAME OVER", Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("GAME OVER") / 2, Metris.metris.getHeight() / 2 - 200);
            g.setFont(new Font("Verdana", Font.BOLD, 20));
            g.setColor(Metris.metris.getSettingsHandler().RAINBOW ? rainbow(1, 1) : Color.WHITE);
            g.drawString("Score: " + score, Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Score: " + score) / 2, Metris.metris.getHeight() / 2 - 170);

            for (Button button : this.buttonList) {
                if (button.getButtonID() != 0) {
                    button.renderButton(g);
                }
            }
        }

        if (!gameOver && !gamePaused) {
            g.setColor(new Color(0, 0, 0, 150));
            g.fillRoundRect(Metris.metris.getWidth() / 2 - 150, Metris.metris.getHeight() - 200, 300, 100, 15, 15);

            g.setColor(Metris.metris.getSettingsHandler().RAINBOW ? rainbow(1, 1) : Color.WHITE);
            g.setFont(new Font("Verdana", Font.BOLD, 20));

            g.drawString(Metris.metris.getPlayerName(), Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth(Metris.metris.getPlayerName()) / 2, Metris.metris.getHeight() - 170);
            g.drawString("Score: " + score, Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Score: " + score) / 2, Metris.metris.getHeight() - 150);
            g.drawString("Versuche: " + Metris.metris.getTries(), Metris.metris.getWidth() / 2 - g.getFontMetrics().stringWidth("Versuche: " + Metris.metris.getTries()) / 2, Metris.metris.getHeight() - 120);
        }

        g.setColor(Color.WHITE);
        g.setFont(new Font("Verdana", Font.BOLD, 10));
        g.drawString("Eingeloggt als: " + Metris.metris.getPlayerName(), Metris.metris.getWidth() - g.getFontMetrics().stringWidth("Eingeloggt als: " + Metris.metris.getPlayerName()) - 15, 15);
    }

    /**
     * Methode für das setzen der nächsten Form
     */
    public void setNextShape() {
        // index Variable erstellen
        int index;

        do {
            // Index = positive Zahl des nächsten zufälligen ints
            index = Math.abs(new Random().nextInt(shapes.length));
        } while (index == lastIndex); // index != lastIndex

        // lastIndex wird auf index gesetzt
        lastIndex = index;

        // nextShape wird eine neue Shape mit der Zufallszahl
        // index zugeordnet.
        nextShape = new Shape(shapes[index].getCoordinates(), shapes[index].getBlock(), this, shapes[index].getColor());
    }

    public void setCurrentShape() {
        currentShape = nextShape;
        setNextShape();

        for (int row = 0; row < currentShape.getCoordinates().length; row++) {
            for (int col = 0; col < currentShape.getCoordinates()[0].length; col++) {
                if (currentShape.getCoordinates()[row][col] != 0) {
                    if (field[currentShape.getY() + row][currentShape.getX() + col] != 0) {
                        if (!submitted) {
                            Metris.metris.getScoreHandler().submitScore(Metris.metris.getPlayerName(), getScore());
                            submitted = true;
                        }
                        gameOver = true;
                    }
                }
            }
        }

    }


    public int[][] getField() {
        return field;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!gamePaused) {
            if (e.getKeyCode() == KeyEvent.VK_UP)
                currentShape.rotateShape();
            if (e.getKeyCode() == KeyEvent.VK_RIGHT)
                currentShape.setDeltaX(1);
            if (e.getKeyCode() == KeyEvent.VK_LEFT)
                currentShape.setDeltaX(-1);
            if (e.getKeyCode() == KeyEvent.VK_DOWN)
                currentShape.speedUp();
        }

        if (e.getKeyCode() == KeyEvent.VK_R)
            if (gameOver) {
                if (!Logger.debug)
                    Metris.metris.getScoreHandler().submitScore(Metris.metris.getPlayerName(), score);
                startGame();
                startMusic();
                gameOver = false;
                Metris.metris.upTries();
            }

        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            if (!gameOver)
                gamePaused = !gamePaused;
        }

        if (e.getKeyCode() == KeyEvent.VK_PLUS)
            if (Metris.metris.getSettingsHandler().MUSIC_VOLUME < 0.9f) {
                Metris.metris.getSettingsHandler().MUSIC_VOLUME += 0.1f;
            }


        if (e.getKeyCode() == KeyEvent.VK_MINUS)
            if (Metris.metris.getSettingsHandler().MUSIC_VOLUME > 0.2f) {
                Metris.metris.getSettingsHandler().MUSIC_VOLUME -= 0.1f;
            }

        if (e.getKeyCode() == KeyEvent.VK_C)
            Metris.metris.getSettingsHandler().RAINBOW = !Metris.metris.getSettingsHandler().RAINBOW;

        if (e.getKeyCode() == KeyEvent.VK_H)
            Metris.metris.getSettingsHandler().GRIDRECT = !Metris.metris.getSettingsHandler().GRIDRECT;

        if (e.getKeyCode() == KeyEvent.VK_1) {
            music.stop();
            music = Metris.metris.getRessourceHandler().loadSound("/music0.wav");
            music.start();
        }
        if (e.getKeyCode() == KeyEvent.VK_2) {
            music.stop();
            music = Metris.metris.getRessourceHandler().loadSound("/music1.wav");
            music.start();
        }
        if (e.getKeyCode() == KeyEvent.VK_3) {
            music.stop();
            music = Metris.metris.getRessourceHandler().loadSound("/music2.wav");
            music.start();
        }
    }

    public void setGamePaused(boolean gamePaused) {
        this.gamePaused = gamePaused;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DOWN)
            currentShape.speedDown();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    public void startGame() {
        stopGame();
        this.looper = new Timer(delay, new GameLooper());
        setNextShape();
        setCurrentShape();
        gameOver = false;
        gamePaused = false;
        submitted = false;
        this.looper.start();
    }

    public void stopGame() {
        score = 0;

        for (int row = 0; row < field.length; row++) {
            for (int col = 0; col < field[row].length; col++) {
                field[row][col] = 0;
            }
        }
        looper.stop();

        if (music != null)
            if (music.isRunning() || music.isActive())
                music.stop();

        this.currentShape = null;
        this.nextShape = null;
    }

    class GameLooper implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            update();
            repaint();
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (gamePaused || gameOver)
            for (Button button : this.buttonList) {
                if (button.isButtonHovered()) {
                    if (button.getButtonID() == 0) {
                        gamePaused = false;
                    } else if (button.getButtonID() == 1) {
                        if (!Logger.debug)
                            Metris.metris.getScoreHandler().submitScore(Metris.metris.getPlayerName(), score);
                        startGame();
                        startMusic();
                        gameOver = false;
                        Metris.metris.upTries();
                    } else if (button.getButtonID() == 2) {
                        System.exit(1337);
                    } else if (button.getButtonID() == 3) {
                        Metris.metris.backToMenu();
                    }
                }
            }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void lineComplete() {
        score += 20;
    }

    public void addScore() {
        score++;
    }

    public int getBoardWidth() {
        return boardWidth;
    }

    public int getBoardHeight() {
        return boardHeight;
    }

    public int getBlockSize() {
        return blockSize;
    }

    public int getScore() {
        return score;
    }

    public static Color rainbow(long offset, float fade) {
        float hue = ((System.nanoTime() + offset) / 10000000000f) % 1;
        long color = Long.parseLong(Integer.toHexString(Color.HSBtoRGB(hue, 1f, 1f)), 16);
        Color c = new Color((int) color);
        return new Color((c.getRed() / 255f) * fade, (c.getGreen() / 255f) * fade, (c.getBlue() / 255f) * fade, c.getAlpha() / 255f);
    }

    public float getVolume() {
        FloatControl gainControl = (FloatControl) music.getControl(FloatControl.Type.MASTER_GAIN);
        return (float) Math.pow(10f, gainControl.getValue() / 20f);
    }

    public void setVolume(float volume) {
        if (volume < 0f || volume > 1f)
            throw new IllegalArgumentException("Volume not valid: " + volume);
        FloatControl gainControl = (FloatControl) music.getControl(FloatControl.Type.MASTER_GAIN);
        gainControl.setValue(20f * (float) Math.log10(volume));
    }
}
