package de.michl.metris.gui;

import de.michl.metris.gui.button.Button;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implemented by: Minuax
 * Implemented since version: 1.3.0
 * Date: 23.05.2020
 **/

public class GUI extends JPanel implements KeyListener, MouseListener, MouseMotionListener {

    public List<Button> buttonList;

    public GUI() {
        this.buttonList = new ArrayList<>();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        for (Button button : this.buttonList) {
            button.setHovered(e.getX(), e.getY());
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        for (Button button : this.buttonList) {
            button.setHovered(e.getX(), e.getY());
        }
    }
}
