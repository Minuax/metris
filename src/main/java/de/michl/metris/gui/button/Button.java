package de.michl.metris.gui.button;

import java.awt.*;

/**
 * Implemented by: Minuax
 * Implemented since version: 1.3.0
 * Date: 23.05.2020
 **/

public class Button {

    private String buttonText; // Variable für den Button-Text

    private int buttonID, buttonX, buttonY, buttonWidth, buttonHeight; // Variabeln für die ID und Position des Button

    private Color buttonColor; // Java-AWT Farbe des Buttons

    private boolean buttonHovered; // Maus über Knopf

    /**
     * Constructor für Button.
     *
     * @param buttonID
     * @param buttonText
     * @param buttonX
     * @param buttonY
     * @param buttonWidth
     * @param buttonHeight
     * @param buttonColor
     */
    public Button(int buttonID, String buttonText, int buttonX, int buttonY, int buttonWidth, int buttonHeight, Color buttonColor) {
        this.buttonID = buttonID;
        this.buttonText = buttonText;
        this.buttonX = buttonX;
        this.buttonY = buttonY;
        this.buttonWidth = buttonWidth;
        this.buttonHeight = buttonHeight;
        this.buttonColor = buttonColor;
    }

    /**
     * Rendermethode für Button
     *
     * @param graphics
     */
    public void renderButton(Graphics graphics) {
        if (buttonHovered) { // Maus über Button?
            graphics.setColor(Color.WHITE); // Farbe auf weiß
            graphics.fillRoundRect(buttonX - 1, buttonY - 1, buttonWidth + 2, buttonHeight + 2, 20, 20); // Rechteck rendern
        }

        graphics.setColor(this.buttonHovered ? buttonColor.brighter() : buttonColor); // Farbe auf 'buttonColor'
        graphics.fillRoundRect(buttonX, buttonY, buttonWidth, buttonHeight, 20, 20); // Rechteck rendern

        graphics.setColor(Color.WHITE); // Farbe auf weiß
        graphics.setFont(new Font("Verdana", Font.BOLD, 30)); // Schriftart ändern
        graphics.drawString(buttonText, buttonX + buttonWidth / 2 - graphics.getFontMetrics().stringWidth(buttonText) / 2, buttonY + buttonHeight / 2 + 10); // Text rendern
    }

    /**
     * Methode um Hovering zu erkennen
     *
     * @param mouseX
     * @param mouseY
     */
    public void setHovered(int mouseX, int mouseY) {
        this.buttonHovered = mouseX >= buttonX && mouseX <= buttonX + buttonWidth && mouseY >= buttonY && mouseY <= buttonY + buttonHeight;
    }

    /**
     * Gibt buttonID zurück
     *
     * @return buttonID
     */
    public int getButtonID() {
        return buttonID;
    }

    /**
     * Gibt buttonHovered zurück
     *
     * @return buttonHovered
     */
    public boolean isButtonHovered() {
        return buttonHovered;
    }
}
