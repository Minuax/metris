package de.michl.metris.ressource;

import de.michl.metris.Metris;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Implemented by: Minuax
 * Implemented since version: 1.0.0
 * Date: 22.05.2020
 **/

public class RessourceHandler {

    private BufferedImage backgroundAsset;

    public RessourceHandler() {
        Metris.metris.getLogger().info("RessourceHandler wurde erfolgreich initialisiert.");

        if (Metris.metris.getSettingsHandler().CUSTOM_BACKGROUD) {
            File customBackground = null;

            for (File file : Metris.metris.getGameDirectory().listFiles()) {
                if (!file.isDirectory()) {
                    if (file.getName().equals(Metris.metris.getSettingsHandler().BACKGROUND_FILENAME)) {
                        customBackground = file;
                    } else if (file.getName().endsWith(".png") || file.getName().endsWith(".jpg")) {
                        if (file.delete()) {
                            Metris.metris.getLogger().info("Deleted unused background file.");
                        }
                    }
                }
            }


            if (customBackground != null) {
                try {
                    this.backgroundAsset = ImageIO.read(customBackground);
                    Metris.metris.getLogger().info("Successfully loaded custom background.");
                } catch (IOException e) {
                    Metris.metris.getLogger().warn("Error while loading custom background.");
                    backgroundAsset = loadImage("/background.png");
                }
            } else {
                backgroundAsset = loadImage("/background.png");
            }
        } else {
            backgroundAsset = loadImage("/background.png");
        }
    }

    public BufferedImage loadImage(String path) {
        try {
            BufferedImage bufferedImage = ImageIO.read(RessourceHandler.class.getResource(path));
            return bufferedImage;
        } catch (IOException e) {
            Metris.metris.getLogger().warn("Fehler beim Laden des Bild unter Pfad: " + path + ". Fehlercode: 11");
            System.exit(11);
        }
        return null;
    }

    public Clip loadSound(String path) {
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(RessourceHandler.class.getResource(path)));
            return clip;
        } catch (Exception e) {
            Metris.metris.getLogger().warn("Fehler beim Laden des Audioclips unter Pfad: " + path + ". Fehlercode: 12");
            System.exit(12);
        }
        return null;
    }

    public void loadStandartBackground() {
        backgroundAsset = loadImage("/background.png");
    }

    public void loadCustomBackground(File file) {
        if (file != null) {
            try {
                this.backgroundAsset = ImageIO.read(file);
                Metris.metris.getLogger().info("Successfully loaded custom background.");
                Metris.metris.getSettingsHandler().CUSTOM_BACKGROUD = true;
                Metris.metris.getSettingsHandler().BACKGROUND_FILENAME = file.getName();
            } catch (IOException e) {
                e.printStackTrace();
                Metris.metris.getLogger().warn("Error while loading custom background.");
                backgroundAsset = loadImage("/background.png");
                Metris.metris.getSettingsHandler().CUSTOM_BACKGROUD = false;
                Metris.metris.getSettingsHandler().BACKGROUND_FILENAME = "";
            }
        } else {
            backgroundAsset = loadImage("/background.png");
        }
    }

    public BufferedImage getBackgroundAsset() {
        return backgroundAsset;
    }
}
