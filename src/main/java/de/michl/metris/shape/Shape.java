package de.michl.metris.shape;

import de.michl.metris.Metris;
import de.michl.metris.gui.impl.GameGUI;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Implemented by: Minuax
 * Implemented since version: 1.0.1
 * Date: 22.05.2020
 **/

public class Shape {


    private final int color;

    private int x, y;

    private long time, lastTime;

    private int delay;

    private final BufferedImage block;

    private int[][] coordinates;

    private int[][] reference;

    private int deltaX;

    private final GameGUI gameGUI;

    private boolean collision = false, moveX = false;

    public Shape(int[][] coordinates, BufferedImage block, GameGUI gameGUI, int color) {
        this.coordinates = coordinates;
        this.block = block;
        this.gameGUI = gameGUI;
        this.color = color;
        deltaX = 0;
        x = 4;
        y = 0;
        delay = 600;
        time = 0;
        lastTime = System.currentTimeMillis();
        reference = new int[coordinates.length][coordinates[0].length];

        System.arraycopy(coordinates, 0, reference, 0, coordinates.length);

    }

    public void update() {
        moveX = true;
        time += System.currentTimeMillis() - lastTime;
        lastTime = System.currentTimeMillis();

        if (collision) {
            for (int row = 0; row < coordinates.length; row++) {
                for (int col = 0; col < coordinates[0].length; col++) {
                    if (coordinates[row][col] != 0)
                        gameGUI.getField()[y + row][x + col] = color;
                }
            }
            checkLine();
            gameGUI.addScore();
            gameGUI.setCurrentShape();
        }

        if (!(x + deltaX + coordinates[0].length > 10) && !(x + deltaX < 0)) {

            for (int row = 0; row < coordinates.length; row++) {
                for (int col = 0; col < coordinates[row].length; col++) {
                    if (coordinates[row][col] != 0) {
                        if (gameGUI.getField()[y + row][x + deltaX + col] != 0) {
                            moveX = false;
                        }

                    }
                }
            }

            if (moveX)
                x += deltaX;

        }

        if (!(y + 1 + coordinates.length > 20)) {

            for (int row = 0; row < coordinates.length; row++) {
                for (int col = 0; col < coordinates[row].length; col++) {
                    if (coordinates[row][col] != 0) {

                        if (gameGUI.getField()[y + 1 + row][x + col] != 0) {
                            collision = true;
                        }
                    }
                }
            }
            if (time > delay) {
                y++;
                time = 0;
            }
        } else {
            collision = true;
        }

        deltaX = 0;
    }

    public void render(Graphics g) {
        if (Metris.metris.getSettingsHandler().GRIDRECT) {
            g.setColor(new Color(149, 149, 149, 149));
            g.fillRect(Metris.metris.getWidth() / 2 - Metris.metris.getGameGui().getBoardWidth() * Metris.metris.getGameGui().getBlockSize() / 2 + x * 30, Metris.metris.getHeight() / 2 - Metris.metris.getGameGui().getBoardHeight() * Metris.metris.getGameGui().getBlockSize() / 2 + y * Metris.metris.getGameGui().getBlockSize() + 30, (coordinates[0].length * Metris.metris.getGameGui().getBlockSize()), 635 - y * Metris.metris.getGameGui().getBlockSize() - 64);
        }

        for (int row = 0; row < coordinates.length; row++) {
            for (int col = 0; col < coordinates[0].length; col++) {
                if (coordinates[row][col] != 0) {
                    g.drawImage(block, Metris.metris.getWidth() / 2 - Metris.metris.getGameGui().getBoardWidth() * Metris.metris.getGameGui().getBlockSize() / 2 + col * 30 + x * 30, Metris.metris.getHeight() / 2 - Metris.metris.getGameGui().getBoardHeight() * Metris.metris.getGameGui().getBlockSize() / 2 + row * 30 + y * 30, null);
                }
            }
        }
    }

    private void checkLine() {
        int size = gameGUI.getField().length - 1;

        for (int x = gameGUI.getField().length - 1; x > 0; x--) {
            int count = 0;
            for (int y = 0; y < gameGUI.getField()[0].length; y++) {
                if (gameGUI.getField()[x][y] != 0)
                    count++;

                gameGUI.getField()[size][y] = gameGUI.getField()[x][y];
            }
            if (count < gameGUI.getField()[0].length) {
                size--;
            } else {
                Metris.metris.getGameGui().lineComplete();
                Metris.metris.getRessourceHandler().loadSound("/line.wav").start();
            }

        }
    }

    public void rotateShape() {
        int[][] rotatedShape = null;

        rotatedShape = transposeMatrix(coordinates);
        rotatedShape = reverseRows(rotatedShape);

        if ((x + rotatedShape[0].length > 10) || (y + rotatedShape.length > 20)) {
            return;
        }

        for (int row = 0; row < rotatedShape.length; row++) {
            for (int col = 0; col < rotatedShape[row].length; col++) {
                if (rotatedShape[row][col] != 0) {
                    if (gameGUI.getField()[y + row][x + col] != 0) {
                        return;
                    }
                }
            }
        }
        coordinates = rotatedShape;
    }


    private int[][] transposeMatrix(int[][] matrix) {
        int[][] temp = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++)
            for (int j = 0; j < matrix[0].length; j++)
                temp[j][i] = matrix[i][j];
        return temp;
    }


    private int[][] reverseRows(int[][] matrix) {
        int middle = matrix.length / 2;

        for (int i = 0; i < middle; i++) {
            int[] temp = matrix[i];

            matrix[i] = matrix[matrix.length - i - 1];
            matrix[matrix.length - i - 1] = temp;
        }

        return matrix;

    }


    public int getColor() {
        return color;
    }

    public void setDeltaX(int deltaX) {
        this.deltaX = deltaX;
    }

    public void speedUp() {
        delay = 50;
    }

    public void speedDown() {
        delay = 600;
    }

    public BufferedImage getBlock() {
        return block;
    }

    public int[][] getCoordinates() {
        return coordinates;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
