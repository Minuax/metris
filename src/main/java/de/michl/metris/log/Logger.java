package de.michl.metris.log;

/**
 * Implemented by: Minuax
 * Implemented since version: 1.0.0
 * Date: 22.05.2020
 **/

public class Logger {

    public static boolean debug = false;

    public void info(String message) {
        if (debug)
            System.out.println("[Tetris|INFO]> " + message);
    }

    public void warn(String message) {
        if (debug)
            System.err.println("[Tetris|WARN]> " + message);
    }

}
