package de.michl.metris.web;

import de.michl.metris.Metris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URL;
import java.util.stream.Collectors;

/**
 * Implemented by: Minuax
 * Implemented since version: 2.1.1
 * Date: 01.06.2020
 **/

public class AuthFrame extends JFrame {

    private JLabel usernameLabel, passwordLabel, statusLabel;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JCheckBox rememberBox;
    private JButton loginButton;

    private boolean loggedIn;

    private File loginFile;

    public AuthFrame() {
        this.setLayout(null);
        this.setTitle("Login");

        this.statusLabel = new JLabel("Bitte einloggen", SwingConstants.CENTER);
        this.statusLabel.setBounds(500 / 2 - 200 / 2, 4, 200, 20);

        this.usernameLabel = new JLabel("Username:", SwingConstants.CENTER);
        this.usernameLabel.setBounds(500 / 2 - 200 / 2, 20, 200, 20);

        this.usernameField = new JTextField();
        this.usernameField.setBounds(500 / 2 - 200 / 2, 40, 200, 20);

        this.passwordLabel = new JLabel("Passwort:", SwingConstants.CENTER);
        this.passwordLabel.setBounds(500 / 2 - 200 / 2, 65, 200, 20);

        this.passwordField = new JPasswordField();
        this.passwordField.setBounds(500 / 2 - 200 / 2, 85, 200, 20);

        this.rememberBox = new JCheckBox("PW Speichern?");
        this.rememberBox.setBounds(500 / 2 - 200 / 2, 120, 200, 20);

        this.loginButton = new JButton("Login");
        this.loginButton.setBounds(500 / 2 - 200 / 2, 180, 200, 35);
        this.loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performLoginAction(WebHandler.loginUser(usernameField.getText(), passwordField.getText()));
            }
        });

        this.setSize(500, 270);
        this.setResizable(false);
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                System.exit(1337);
            }
        });
        this.setLocationRelativeTo(null);

        this.add(usernameLabel);
        this.add(usernameField);
        this.add(passwordLabel);
        this.add(passwordField);
        this.add(rememberBox);
        this.add(statusLabel);
        this.add(loginButton);

        File dataDir = new File(System.getenv("APPDATA"), "Metris");

        if (!dataDir.isDirectory()) {
            dataDir.mkdirs();
        }

        loginFile = new File(dataDir, "login.auth");

        if (readLoginFile() != null) {
            performFileLogin(WebHandler.loginUser(readLoginFile()[0], readLoginFile()[1]));
        } else {
            this.setVisible(true); // JFrame sichtbar machen
        }
    }

    private String performLoginAction(String rs) {
        String result = "";

        try {
            switch (rs) {
                case "success":

                    if (this.rememberBox.isSelected()) {
                        if (loginFile.exists()) {
                            if (loginFile.delete()) {
                                System.err.println("LoginFile already exists, deleting it.");
                            }
                        }

                        try {
                            loginFile.createNewFile();

                            BufferedWriter br = new BufferedWriter(new FileWriter(loginFile.getAbsolutePath()));
                            br.write(this.usernameField.getText() + ":" + this.passwordField.getText());
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    this.statusLabel.setText("Erfolgreich eingeloggt.");
                    AuthFrame.this.setVisible(false);
                    new Metris(AuthFrame.this.usernameField.getText());

                    break;
                case "notfound":
                    this.statusLabel.setText("Dieser Nutzer existiert nicht.");

                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                                }
                            },
                            5000
                    );
                    break;
                case "wrongpw":
                    this.statusLabel.setText("Passwort falsch.");

                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                                }
                            },
                            5000
                    );
                    break;
                case "notfoundwrongpw":
                    this.statusLabel.setText("Alles falsch.");

                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                                }
                            },
                            5000
                    );
                    break;
                default:
                    this.statusLabel.setText("Fehler. Internetverbindung?");

                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                                }
                            },
                            5000
                    );
            }
        } catch (Exception exception) {
            this.statusLabel.setText("Fehler. Internetverbindung?");

            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                        }
                    },
                    5000
            );
        }
        return result;
    }

    private String performFileLogin(String rs) {
        String result = "";

        try {
            switch (rs) {
                case "success":
                    this.statusLabel.setText("Erfolgreich eingeloggt.");
                    AuthFrame.this.setVisible(false);
                    new Metris(readLoginFile()[0]);

                    break;
                case "notfound":
                    this.statusLabel.setText("Dieser Nutzer existiert nicht.");

                    AuthFrame.this.setVisible(true);

                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                                }
                            },
                            5000
                    );
                    break;
                case "wrongpw":
                    this.statusLabel.setText("Passwort falsch.");

                    AuthFrame.this.setVisible(true);

                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                                }
                            },
                            5000
                    );
                    break;
                case "notfoundwrongpw":
                    this.statusLabel.setText("Alles falsch.");

                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                                }
                            },
                            5000
                    );
                    break;
                default:
                    this.statusLabel.setText("Fehler. Internetverbindung?");

                    AuthFrame.this.setVisible(true);

                    new java.util.Timer().schedule(
                            new java.util.TimerTask() {
                                @Override
                                public void run() {
                                    AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                                }
                            },
                            5000
                    );
            }
        } catch (Exception exception) {
            this.statusLabel.setText("Fehler. Internetverbindung?");

            AuthFrame.this.setVisible(true);

            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            AuthFrame.this.statusLabel.setText("Bitte einloggen.");
                        }
                    },
                    5000
            );
        }
        return result;
    }

    public String[] readLoginFile() {
        try {
            if (loginFile.exists()) {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(loginFile.getAbsolutePath()));
                return bufferedReader.readLine().split(":");
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }
}
