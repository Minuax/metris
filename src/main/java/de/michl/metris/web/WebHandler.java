package de.michl.metris.web;

import de.michl.metris.Metris;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Implemented by: Minuax
 * Implemented since version: 2.3.0
 * Date: 03.06.2020
 **/

public class WebHandler {

    public static String loginUser(String username, String password) {
        String result = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((new URL("http://ni2883569-2.web17.nitrado.hosting/login.php?username=" + username + "&password=" + password)).openStream()));
            result = bufferedReader.readLine();

            bufferedReader.close();
        } catch (IOException e) {
            Metris.metris.getLogger().warn("Could not login, please check your internet connection.");
        }
        return result;
    }

    public boolean checkVersion() {
        String onlineVersion = "";
        boolean newerVersion = false;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((new URL("http://ni2883569-2.web17.nitrado.hosting/version.txt")).openStream()));
            onlineVersion = bufferedReader.readLine();

            String[] onlineDigit = onlineVersion.split("\\.");
            String[] localDigit = Metris.metris.getVersion().split("\\.");

            if (Integer.parseInt(onlineDigit[0]) > Integer.parseInt(localDigit[0]) || Integer.parseInt(onlineDigit[1]) > Integer.parseInt(localDigit[1]) || Integer.parseInt(onlineDigit[2]) > Integer.parseInt(localDigit[2])) {
                newerVersion = true;
            }

            bufferedReader.close();
        } catch (IOException ioException) {
            Metris.metris.getLogger().warn("Could not check version, check your internet-connection");
        }
        return newerVersion;
    }

    public List<String> readChangelog() {
        List<String> lines = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((new URL("http://ni2883569-2.web17.nitrado.hosting/changelog.txt")).openStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.startsWith("<"))
                    continue;

                lines.add(line);
            }

            bufferedReader.close();
        } catch (IOException e) {
            Metris.metris.getLogger().warn("Could not read changelog, please check your internet connection.");
        }
        return lines;
    }

}
